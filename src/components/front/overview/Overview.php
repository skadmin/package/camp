<?php

declare(strict_types=1);

namespace Skadmin\Camp\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Skadmin\Camp\BaseControl;
use Skadmin\Camp\Doctrine\Camp\CampFacade;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Overview extends TemplateControl
{
    use APackageControl;

    /** @var CampFacade */
    private $facade;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(CampFacade $facade, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
    }

    public function getTitle(): string
    {
        return 'camp.front.overview.title';
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');

        $template->camps    = $this->facade->getCampsInFuture();
        $template->oldCamps = $this->facade->getCampsInPast();
        $template->package  = new BaseControl();

        $template->render();
    }
}
