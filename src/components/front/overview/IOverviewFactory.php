<?php

declare(strict_types=1);

namespace Skadmin\Camp\Components\Front;

/**
 * Interface IOverviewFactory
 */
interface IOverviewFactory
{
    public function create() : Overview;
}
