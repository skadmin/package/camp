<?php

declare(strict_types=1);

namespace Skadmin\Camp\Components\Traits;

use Skadmin\Camp\Doctrine\File\FileFacade;
use Skadmin\FileStorage\FileStorage;

/**
 * Class TFile
 */
trait TFile
{
    /** @var FileStorage */
    private $fileStorage;

    /** @var FileFacade */
    private $facadeFile;

    public function injectFileStorage(FileStorage $fileStorage) : void
    {
        $this->fileStorage = $fileStorage;
    }

    public function injectFacadeFile(FileFacade $facadeFile) : void
    {
        $this->facadeFile = $facadeFile;
    }
}
