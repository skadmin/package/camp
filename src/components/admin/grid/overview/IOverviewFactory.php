<?php

declare(strict_types=1);

namespace Skadmin\Camp\Components\Admin;

/**
 * Interface IOverviewFactory
 */
interface IOverviewFactory
{
    public function create() : Overview;
}
