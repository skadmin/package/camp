<?php

declare(strict_types=1);

namespace Skadmin\Camp\Components\Admin;

use Skadmin\Camp\Doctrine\Camp\Camp;

/**
 * Interface IOverviewFactory
 */
interface IStaffFactory
{
    public function create(Camp $camp) : Staff;
}
