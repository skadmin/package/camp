<?php

declare(strict_types=1);

namespace Skadmin\Camp\Components\Admin;

/**
 * Interface IDetailFactory
 */
interface IDetailFactory
{
    public function create(int $id) : Detail;
}
