<?php

declare(strict_types=1);

namespace Skadmin\Camp\Components\Admin;

use App\Components\Grid\TemplateControl;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\Security\User as LoggedUser;
use Skadmin\Camp\Doctrine\Camp\Camp;
use Skadmin\Camp\Doctrine\Camp\CampFacade;
use Skadmin\Camp\Doctrine\Staff\StaffFacade;
use Skadmin\File\Components\Admin\FileDownloadByFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Detail extends TemplateControl
{
    use APackageControl;
    use FileDownloadByFacade;

    /** @var CampFacade */
    private $facade;

    /** @var StaffFacade */
    private $facadeStaff;

    /** @var UserFacade */
    private $facadeUser;

    /** @var User */
    private $user;

    /** @var Camp */
    private $camp;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(int $id, CampFacade $facade, StaffFacade $facadeStaff, UserFacade $facadeUser, LoggedUser $user, Translator $translator, ImageStorage $imageStorage, FileStorage $fileStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->facadeStaff  = $facadeStaff;
        $this->facadeUser   = $facadeUser;
        $this->imageStorage = $imageStorage;

        $this->user        = $this->facadeUser->get($user->getId());
        $this->camp        = $this->facade->get($id);
        $this->fileObject  = $this->camp;
        $this->fileStorage = $fileStorage;
    }

    public function getTitle() : SimpleTranslation
    {
        return new SimpleTranslation('extract.camp.detail.title - %s', $this->camp->getName());
    }

    public function render() : void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/detail.latte');

        $template->camp       = $this->camp;
        $template->loggedUser = $this->user;

        $template->render();
    }

    public function handleLogin(int $id) : void
    {
        if ($this->facadeStaff->addUserToStaff($id, $this->user)) {
            $this->onFlashmessage('extract.camp.detail.flash.success.login', Flash::SUCCESS);
        } else {
            $this->onFlashmessage('extract.camp.detail.flash.danger.login', Flash::DANGER);
        }

        $this->redrawDetail();
    }

    private function redrawDetail() : void
    {
        $this->redrawControl('snipStaff');
        $this->redrawControl('snipNumberOfStaff');
    }

    public function handleLogout(int $id) : void
    {
        $this->facadeStaff->removeUserFromStaff($id, $this->user);
        $this->onFlashmessage('extract.camp.detail.flash.success.logout', Flash::SUCCESS);
        $this->redrawDetail();
    }

    public function handleLogoutUser(int $id, int $userId) : void
    {
        $this->facadeStaff->removeUserFromStaff($id, $userId);
        $this->onFlashmessage('extract.camp.detail.flash.success.logout-user', Flash::SUCCESS);
        $this->redrawDetail();
    }
}
