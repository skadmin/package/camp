<?php

declare(strict_types=1);

namespace Skadmin\Camp\Doctrine\Staff;

use SkadminUtils\DoctrineTraits\Facade;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Camp\Doctrine\Camp\Camp;

/**
 * Class CampFacade
 */
final class StaffFacade extends Facade
{
    /** @var UserFacade */
    private $facadeUser;

    public function __construct(EntityManagerDecorator $em, UserFacade $facadeUser)
    {
        parent::__construct($em);
        $this->table = Staff::class;

        $this->facadeUser = $facadeUser;
    }

    public function getModelForCamp(Camp $camp): QueryBuilder
    {
        $qb = $this->getModel();
        $qb->where('a.camp = :camp')
            ->setParameter('camp', $camp);

        return $qb;
    }

    public function create(Camp $camp, string $name, string $description, int $number, int $financialReward, string $financialPremium): Staff
    {
        return $this->update(null, $camp, $name, $description, $number, $financialReward, $financialPremium);
    }

    public function update(?int $id, Camp $camp, string $name, string $description, int $number, int $financialReward, string $financialPremium): Staff
    {
        $staff = $this->get($id);
        $staff->update($camp, $name, $description, $number, $financialReward, $financialPremium);

        $this->em->persist($staff);
        $this->em->flush();

        return $staff;
    }

    public function get(?int $id = null): Staff
    {
        if ($id === null) {
            return new Staff();
        }

        $user = parent::get($id);

        if ($user === null) {
            return new Staff();
        }

        return $user;
    }

    /**
     * @param int|Staff $staff
     * @param int|User $user
     */
    public function addUserToStaff($staff, $user): bool
    {
        if (! $staff instanceof Staff) {
            $staff = $this->get($staff);
        }

        if (! $user instanceof User) {
            $user = $this->facadeUser->get($user);
        }

        if ($staff->addUser($user)) {
            $this->em->flush();
            return true;
        }

        return false;
    }

    /**
     * @param int|Staff $staff
     * @param int|User $user
     */
    public function removeUserFromStaff($staff, $user): void
    {
        if (! $staff instanceof Staff) {
            $staff = $this->get($staff);
        }

        if (! $user instanceof User) {
            $user = $this->facadeUser->get($user);
        }

        $staff->removeUser($user);
        $this->em->flush();
    }

    public function remove(Staff $staff): void
    {
        $this->em->remove($staff);
        $this->em->flush();
    }
}
