<?php

declare(strict_types=1);

namespace Skadmin\Camp\Doctrine\Staff;

use SkadminUtils\DoctrineTraits\Entity;
use App\Model\Doctrine\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Skadmin\Camp\Doctrine\Camp\Camp;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'camp_staff')]
#[ORM\HasLifecycleCallbacks]
class Staff
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Description;
    use Entity\Number;
    use Entity\FinancialReward;
    use Entity\FinancialPremium;

    #[ORM\ManyToOne(targetEntity: Camp::class, inversedBy: 'staff')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Camp $camp;

    /** @var ArrayCollection<int, User> */
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: 'camp_staff_user')]
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function update(Camp $camp, string $name, string $description, int $number, int $financialReward, string $financialPremium) : void
    {
        $this->camp        = $camp;
        $this->name        = $name;
        $this->description = $description;
        $this->number      = $number;

        $this->financialReward  = $financialReward;
        $this->financialPremium = $financialPremium;
    }

    public function addUser(User $user) : bool
    {
        if ($this->isUserLoggedInCamp($user)) {
            return false;
        }

        if ($this->getNumberOfLoggedUsers() >= $this->getNumber()) {
            return false;
        }

        $this->users->add($user);
        return true;
    }

    public function isUserLoggedInCamp(User $user) : bool
    {
        /** @var Staff $staff */
        foreach ($this->getCamp()->getStaff() as $staff) {
            if ($staff->getUsers()->contains($user)) {
                return true;
            }
        }

        return false;
    }

    public function getCamp() : Camp
    {
        return $this->camp;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function getNumberOfLoggedUsers() : int
    {
        return $this->users->count();
    }

    public function removeUser(User $user) : void
    {
        $this->users->removeElement($user);
    }

    public function isUserLogged(User $user) : bool
    {
        return $this->users->contains($user);
    }
}
