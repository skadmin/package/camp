<?php

declare(strict_types=1);

namespace Skadmin\Camp\Doctrine\Camp;

use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\File\Doctrine\File\File;

/**
 * Class CampFacade
 */
final class CampFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Camp::class;
    }

    public function create(string $name, int $numberOfChildren, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview) : Camp
    {
        return $this->update(null, $name, $numberOfChildren, $content, $termFrom, $termTo, $placName, $placeAddress, $placeGpsLat, $placeGpsLng, $imagePreview);
    }

    public function update(?int $id, string $name, int $numberOfChildren, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview) : Camp
    {
        $camp = $this->get($id);
        $camp->update($name, $numberOfChildren, $content, $termFrom, $termTo, $placName, $placeAddress, $placeGpsLat, $placeGpsLng, $imagePreview);

        $this->em->persist($camp);
        $this->em->flush();

        return $camp;
    }

    public function get(?int $id = null) : Camp
    {
        if ($id === null) {
            return new Camp();
        }

        $user = parent::get($id);

        if ($user === null) {
            return new Camp();
        }

        return $user;
    }

    /**
     * @return Camp[]
     */
    public function getCampsInFuture(?int $limit = null) : array
    {
        $qb = $this->getModel();
        $qb->where('a.termFrom >= :termFrom')
            ->setParameter('termFrom', (new DateTime())->setTime(0, 0, 0))
            ->orderBy('a.termFrom');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Camp[]
     */
    public function getCampsInPast(?int $limit = null) : array
    {
        $qb = $this->getModel();
        $qb->where('a.termTo < :termTo')
            ->setParameter('termTo', (new DateTime())->setTime(0, 0, 0))
            ->orderBy('a.termTo', 'DESC');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function addFile(Camp $camp, File $file) : void
    {
        $camp->addFile($file);
        $this->em->flush();
    }

    public function getFileByHash(Camp $camp, string $hash) : ?File
    {
        return $camp->getFileByHash($hash);
    }

    public function removeFileByHash(Camp $camp, string $hash) : void
    {
        $camp->removeFileByHash($hash);
        $this->em->flush();
    }
}
