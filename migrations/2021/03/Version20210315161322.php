<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210315161322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'camp.overview', 'hash' => '81f5db356e3dbc8aa38786df9beb92bb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Tábory', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.camp.title', 'hash' => 'd3dcdef5edd2ab38b412d6aece985c6d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Tábory', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.camp.description', 'hash' => '9e683e305f42fd1257e6f2567b2ce12b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat tábory', 'plural1' => '', 'plural2' => ''],
            ['original' => 'camp.overview.title', 'hash' => 'd751f48cbe01a0cbae9d07509bc408ad', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Tábory|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.action.new', 'hash' => 'aa66890d8b1a2a4499ab9bcac7874139', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit tábor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.name', 'hash' => 'b82666660061cf27b188766c63b31cf7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.place-name', 'hash' => '4276ae7f993bee0d1686b6e7e77d2bec', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Místo konání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.term', 'hash' => '9ebd783771b31a6a0b39b72471d9b11d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.number-of-children', 'hash' => '236c74d734efefeec289ec98c72ecc58', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.number-of-staff', 'hash' => 'dbcb651e3959a336770098bf2c33b5cb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet personálu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.total-number-of-people', 'hash' => 'df7f30fa44651c243c8c8c7adf010b09', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Celkový počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'camp.edit.title', 'hash' => '1aeacc5d4213b80e6abdcdab15274397', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení tábora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.name', 'hash' => '373e10e1621cb49a0a82efea5eef79b2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.name.req', 'hash' => '656615f8f223a776c03453e621d90a2a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.number-of-children', 'hash' => 'b9056b4740ad9353a4beae9e10d96ded', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.number-of-children.req', 'hash' => 'aa6ebb0b2fc8cbfccaa8c02a3c11876a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.term', 'hash' => 'd6f1eed46841ca3e046652cda05e4fd5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.term.req', 'hash' => '589063fc6c214086ca883bc5bff9a996', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.place-name', 'hash' => '131b3698f983d8ae85d80c264695a9e6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název místa konání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.place-address', 'hash' => 'de633db6a87485528576a9fe66882612', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.place-gps-lat', 'hash' => '8a37dbce7bcff1e9931349c532ce8836', 'module' => 'admin', 'language_id' => 1, 'singular' => 'GPS šířka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.place-gps-lng', 'hash' => '0e8865acef06a3f773c50f99be4011c9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'GPS výška', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.image-preview', 'hash' => 'c37491b04e8d5bf6ddb17d972d7c9d12', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled tábora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.image-preview.rule-image', 'hash' => 'ea62982dbbb67bd3be28e2d638cde5ba', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.content', 'hash' => 'ae498e365a04ed5c367b70bec3cf8e33', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.send', 'hash' => '4af666ec16398ae7f322ed9c3ba2a99d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.send-back', 'hash' => '2307565aac1c9e8a4aa64935b1b72d8c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.back', 'hash' => '0fef3f59c87c6ab08d654bae6de081db', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'camp.edit.title - %s', 'hash' => '036373b5cea68e9f33fa64221ccf1bef', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace tábora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.flash.success.create', 'hash' => '363152ca28107c9102b81810de286168', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Tábor byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.staff.action.add', 'hash' => 'e9b9c052b109a1299f37e1aaa2cbccf1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přidat personál', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.staff.name', 'hash' => '4bcf6ea4f5af36cd2c1d6583d33091ee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pozice', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.staff.description', 'hash' => '94ddf9c490f56c017ad11e91134e756c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.staff.number', 'hash' => 'de4ee4609f4c5aea11a9efd521d1403a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.staff.financial-reward', 'hash' => 'dabc413a3f6bd4abca953ead152d9b7a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odměna', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.staff.financial-premium', 'hash' => '83ae579ebf6d71f7ae329b83d05ff5e5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Prémie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.flash.success.update', 'hash' => 'bc1e8ae003d5fbf65763499a927a4be9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Tábor byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.action.edit', 'hash' => '91f6c35d56690e246b2b0406d7e7d761', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp.overview.action.detail', 'hash' => '632b9770a6d5cc790f926408ffaa5869', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.file.name', 'hash' => '17004b488d45a28b696a7b20ecb69b6d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.file.size', 'hash' => 'cc7d2966f2d897af403f329284b0cff0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.file.last-update-at', 'hash' => 'c3acbfd51e7a413f4328b8a316ad99dd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.file.action.download', 'hash' => '3103dac556a80238b0c32e55d59107c4', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.file.remove.confirm', 'hash' => '96c035f92c6cfbdee6b0fbda29c33f9e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat dokument?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp.edit.file.action.remove', 'hash' => 'afa856edd8c96d9b2adcc42a34d83e0b', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.title - %s', 'hash' => 'e68671f9ab448fff489e2e469993cac1', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Detail tábora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.term', 'hash' => 'd3c9beed35d796b71876f24a9b2cc310', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.number-of-children', 'hash' => 'cbd1860f8ee25496fcd387dee9e1beee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.number-of-staff', 'hash' => 'f35cfdf6aa9d34b0a47cba0fd7113e04', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet personálu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.total-number-of-people', 'hash' => '81a5e856675485585b549bc63d490331', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Celkový počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.title', 'hash' => '5eec7f6c24696996c2093fae60f72a12', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Personál', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.name', 'hash' => '8e4bb16e75ac9fc198eb2f51048619a8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pozice', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.financial-reward', 'hash' => '20ab4e45ed4df0d3e0d23f2d0a1d9a1f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odměna', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.financial-premium', 'hash' => 'ba553c5ab697fd24747822213b4b6c00', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Prémie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.logged', 'hash' => 'e7f0a350908c5e2a065fbb4fc8ad4f21', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsazenost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.log-in', 'hash' => '4bdbf3ede8a2d387005d974fa5b51986', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přihlásit se', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.files.title', 'hash' => '7ad60ea9c83f3ff25dc95fc3882f8dd3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokumenty', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.file.name', 'hash' => 'f3f7c64f14181efbc555f6648d79923f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.file.size', 'hash' => '7a99f82f2cc12316a68514df4b1c88f1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.file.last-update-at', 'hash' => '64ab20e43ebdbb2d36a6f79caec3f470', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.file.action.download', 'hash' => '13ff313cad5522cd198a8c5f2119c405', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.content.title', 'hash' => '3c1db422320560f4d42ba0832d5cae66', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.place', 'hash' => '823525c01f39e90545271a67c5e90c8f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.flash.success.login', 'hash' => 'a27959a3044951bedcb2723c6437b549', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste se přihlásili.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.log-out-user.confirm', 'hash' => 'a8b337e5d81c4a0a6b29989390c4ed07', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat účastníka?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.log-out.confirm', 'hash' => '1850d18e6e07f013c20d3d68f9c26434', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu se chcete odhlásit?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.staff.log-out', 'hash' => '00163bee168cd5d47632cbd406bf09e8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odhlásit se', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.flash.success.logout', 'hash' => '79de28c5e523feb47303f8f9416c7dfc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste se odhlásili.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.camp.detail.flash.success.logout-user', 'hash' => 'd786eec3357717e0776e71f9c7bddc34', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste odhlásili účastníka.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
