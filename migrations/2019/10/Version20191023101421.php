<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191023101421 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE camp_staff_user (staff_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9FBE98A4D4D57CD (staff_id), INDEX IDX_9FBE98A4A76ED395 (user_id), PRIMARY KEY(staff_id, user_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE camp_staff_user ADD CONSTRAINT FK_9FBE98A4D4D57CD FOREIGN KEY (staff_id) REFERENCES camp_staff (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE camp_staff_user ADD CONSTRAINT FK_9FBE98A4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE staff_user');
    }
}
