<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191017105917 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE camp (id INT AUTO_INCREMENT NOT NULL, number_of_children INT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, term_from DATETIME NOT NULL, term_to DATETIME NOT NULL, place_name VARCHAR(255) NOT NULL, place_address VARCHAR(255) NOT NULL, place_gps_lat VARCHAR(255) NOT NULL, place_gps_lng VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');

        /* DATA */
        $resources = [
            [
                'name'        => 'camp',
                'title'       => 'role-resource.camp.title',
                'description' => 'role-resource.camp.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE camp');
    }
}
